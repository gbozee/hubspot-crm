"""test_crm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.http import JsonResponse, HttpResponse
from . import HubspotAPI
from .models import HubspotStorage
from django.shortcuts import reverse
from django.views.decorators.csrf import csrf_exempt
from .signals import hubspot_action_after_message_webhook


def hubspot_auth_response(request):
    hub = HubspotAPI(reverse('hubspot:hubspot_code'))
    code = request.GET.get('code')
    error = request.GET.get('error')
    description = request.GET.get('error_description')
    response = {
        'code': code,
        'error': error,
        'error_description': description
    }
    response, status = hub.get_token(code)
    if status < 400:
        HubspotStorage.save_token(**response)
    response.update(status=status)
    print(response)
    return JsonResponse(response)


def hubspot_authorize(request):
    hub = HubspotAPI(reverse('hubspot:hubspot_code'))

    return HttpResponse('''
    <a href="{}">Authorize hubspot</a>
    '''.format(hub.get_authorization_url()))


def mailgun_open(request):
    get_req = request.POST
    params = {
        'event': get_req.get('event'),
        'recipient': get_req.get('recipient'),
        'timestamp': get_req.get('timestamp'),
        'tag': get_req.get('tag')
    }
    hubspot_action_after_message_webhook.send(
        sender=HubspotStorage, params=params)
    return JsonResponse(params)


urlpatterns = [
    url(r'^mailgun/webhook/open/$', csrf_exempt(mailgun_open), name="mailgun_open"),
    url(r'^auth-response/$', hubspot_auth_response, name="hubspot_code"),
    url(r'^authorize/$', hubspot_authorize, name="hubspot_authorize"),
]
