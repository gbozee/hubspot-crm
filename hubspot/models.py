from django.db import models
from django.utils import timezone


class HubspotOwner(models.Model):
    SALES = 1
    CUSTOMER_SUCCESS = 2
    TEAMS = ((SALES, "Sales Team"),
    (CUSTOMER_SUCCESS, "Customer Success Team"))
    email = models.EmailField()
    hubspot_id = models.IntegerField()
    team = models.IntegerField(default=SALES,choices=TEAMS)

    def __repr__(self):
        return "<HubspotOwner: {}>".format(self.email)

    def __str__(self):
        return "{} {}".format(self.email, self.team)


class NoContactException(Exception):
    pass


class HubspotContact(models.Model):
    email = models.EmailField()
    vid = models.IntegerField()

    def __repr__(self):
        return "<HubspotContact: {}>".format(self.email)

    @classmethod
    def bulk_update_record(cls, records, field='vid'):
        for r in records:
            if not cls.objects.filter(email=r.email).exists():
                cls.objects.get_or_create(email=r.email,
                vid=getattr(r,field))


    @classmethod
    def get_contact_id(cls, email, extra_check=lambda: None):
        record = cls.objects.filter(email=email).first()
        if record:
            return record.vid
        result = extra_check()
        if result:
            cls.objects.get_or_create(email=email, vid=result)
            return result
        raise NoContactException(
            "The contact doesn't exist on hubspot. please create")


class HubspotStorage(models.Model):
    refresh_token = models.CharField(max_length=60, blank=True, null=True)
    access_token = models.CharField(max_length=60, blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True)
    contact_properties = models.BooleanField(default=False)
    fetched_agents = models.BooleanField(default=False)
    deal_properties = models.BooleanField(default=False)
    deal_group_name = models.CharField(
        max_length=30, default="lesson_information_details")
    booking_pipeline_id = models.CharField(
        max_length=60, blank=True
    )

    @classmethod
    def save_token(cls, **kwargs):
        cls.objects.all().delete()
        return cls.objects.create(refresh_token=kwargs['refresh_token'],
                                  access_token=kwargs['access_token'])

    def has_expired(self):
        timestamp = timezone.now() - self.date_added
        in_hours = timestamp.total_seconds() / 3600
        return in_hours > 6

    @classmethod
    def get_token(cls):
        return cls.objects.first()
