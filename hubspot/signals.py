from django.dispatch import Signal

hubspot_action_after_message_webhook = Signal(providing_args=["params"])
