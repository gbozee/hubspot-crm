from django.conf import settings

HUBSPOT_CLIENT_ID = getattr(settings, "HUBSPOT_CLIENT_ID", None)
HUBSPOT_CLIENT_SECRET = getattr(settings, 'HUBSPOT_CLIENT_SECRET', None)
HUBSPOT_REDIRECT_URL = getattr(settings, 'HUBSPOT_REDIRECT_URL', "")
HUBSPOT_SCOPES = getattr(settings, "HUBSPOT_SCOPES", None)
HUBSPOT_API_KEY = getattr(settings, 'HUBSPOT_API_KEY', None)
ALLOWED_HOSTS = getattr(settings, 'ALLOWED_HOSTS', [])
HUBSPOT_EMAIL_WEBHOOK = getattr(settings, 'HUBSPOT_EMAIL_WEBHOOK', None)
if HUBSPOT_REDIRECT_URL:
    ALLOWED_HOSTS.append(HUBSPOT_REDIRECT_URL.split(
        "https://")[1])
if HUBSPOT_EMAIL_WEBHOOK:
    ALLOWED_HOSTS.append(HUBSPOT_EMAIL_WEBHOOK)
