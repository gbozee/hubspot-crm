# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
from builtins import super, str, int
from future.standard_library import install_aliases
install_aliases()
from future.moves.itertools import zip_longest
from . import settings
import requests
from decimal import Decimal
import pytz
import datetime
from urllib.parse import urlparse, urlencode, quote
from urllib.request import urlopen, Request
from urllib.error import HTTPError
import time
from collections import OrderedDict
from django.utils import timezone


def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def to_timestamp(tt):
    loc = pytz.utc.localize(tt) if not tt.tzinfo else tt
    result = (loc - pytz.utc.localize(datetime.datetime(1970, 1, 1)))\
        .total_seconds() * 1000
    return int(result)


def as_timestamp(tt):
    return to_timestamp(tt)


class HubspotAPIException(Exception):
    pass


def single_field(key, value, field=None):
    new_field = field
    if not field:
        new_field = 'name'
    new_value = value
    if type(value) == datetime.datetime:
        new_value = as_timestamp(value)
    elif type(value) == Decimal:
        new_value = str(value)
    elif type(value) == list:
        new_value = ", ".join(value)
    # elif not value:
    #     new_value = None
    else:
        if not value:
            new_value = ""
        new_value = str(value)
    if key == 'days_per_week':
        new_value = "{} weeks".format(new_value)
    return {
        new_field: key,
        'value': new_value}


def get_property_array(ss, obj, field=None):
    return [single_field(value[0], getattr(obj, value[1]), field=field) for value in ss]


def initialize_deal_property_array(field):
    return [{
            "name": key,
            "label": value.title(),
            "description": "",
            "type": "string",
            "fieldType": "text"
            } for key, value in field.items()]


class HubspotAPI(object):
    def __init__(self, redirect_uri, use_api_key=True):
        self.client_id = settings.HUBSPOT_CLIENT_ID
        self.client_secret = settings.HUBSPOT_CLIENT_SECRET
        self.redirect_url = settings.HUBSPOT_REDIRECT_URL
        self.base_url = "https://app.hubspot.com"
        self.call_url = "https://api.hubapi.com"
        self.view_url = redirect_uri
        self.api_key = None
        if use_api_key:
            self.api_key = settings.HUBSPOT_API_KEY
            if not self.api_key:
                raise HubspotAPIException(
                    "Please set the HUBSPOT_API_KEY env variable")
        else:
            if not self.client_id and self.client_secret and self.redirect_url:
                raise HubspotAPIException(
                    ("Please remember to set the HUBSPOT_CLIENT_ID "
                     "AND HUBSPOT_CLIENT_SECRET env"))
        self.redirect_url = self.redirect_url + redirect_uri

    def get_path(self, path, call=False):
        url = self.base_url
        if call:
            url = self.call_url
        return "{}{}".format(url, path)

    def get_authorization_url(self):
        path = self.get_path("/oauth/authorize")
        scope = quote(settings.HUBSPOT_SCOPES)
        return "{}?redirect_uri={}&client_id={}&scope={}".format(
            path, self.redirect_url, self.client_id, scope)

    def make_request(self, method, path, **kwargs):
        options = {
            'GET': requests.get,
            'POST': requests.post,
            'PUT': requests.put,
            'DELETE': requests.delete
        }
        call = kwargs.pop('call', True)
        url = self.get_path(path, call=call)
        if method in ['POST', 'PUT']:
            headers = kwargs.get('headers')
            if headers.get('content-type') == 'application/json':
                json = kwargs.pop("data", None)
                kwargs.update(json=json)
        return options[method](url, **kwargs)

    # Todo: Test this
    def call_api(self, method, path,  **kwargs):
        headers = kwargs.pop('headers', {})
        params = kwargs.pop('params', {})
        if self.api_key:
            params.update(hapikey=self.api_key)
            headers.update({'content-type': "application/json"})
        else:
            headers.update(Authorization="Bearer {}".format(
                self.fetch_token_from_db()))
        return self.make_request(method, path, params=params, headers=headers, **kwargs)

    def _get_token_details(self, grant_type, param):
        options = {
            'authorize': ("authorization_code", 'code'),
            'refresh': ('refresh_token', 'refresh_token')
        }
        data = options[grant_type]
        params = {
            "grant_type": data[0],
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "redirect_uri": self.redirect_url,
            data[1]: param
        }
        headers = {
            'content-type': 'application/x-www-form-urlencoded'
        }
        response = self.make_request(
            'POST', '/oauth/v1/token', data=params, headers=headers)
        # import pdb
        # pdb.set_trace()
        return response.json(), response.status_code

    def get_token(self, code):
        return self._get_token_details("authorize", code)

    def refresh_token(self, refresh_token):
        return self._get_token_details('refresh', refresh_token)

    def fetch_token_from_db(self):
        from .models import HubspotStorage

        result = HubspotStorage.get_token()
        if not result:
            raise HubspotAPIException(
                ("Token not available, "
                 "please visit the {} url.").format(self.view_url))
        if result.has_expired():
            token, status = self.refresh_token(result.refresh_token)
            if status < 400:
                result = HubspotStorage.save_token(**token)
            else:
                raise HubspotAPIException("code: {}, description: {}".format(
                    token['error'], token['error_description']))
        return result.access_token

    def get_contact_properties(self):
        token = self.fetch_token_from_db()
        headers = {
            "Authorization": "Bearer {}".format(token)
        }
        response = self.make_request(
            "GET", "/properties/v1/contacts/properties")

    def _get_owners(self):
        response = self.call_api('GET', '/owners/v2/owners/')
        if response.status_code < 400:
            return response.json()

    def _create_customer_deal_pipeline(self):
        pass

    def _create_deal_pipeline(self):
        data = {
            "label": "Client Request Pipeline",
            "displayOrder": 0,
            "stages": [
                {
                    "stageId": "appointmentscheduled",
                    "label": "Qualified To Pay",
                    "probability": 0.1,
                    "active": True,
                    "displayOrder": 0,
                    "closedWon": False
                },
                {
                    "stageId": "qualifiedtobuy",
                    "label": "Sent Tutor Profile To Client",
                    "probability": 0.3,
                    "active": True,
                    "displayOrder": 1,
                    "closedWon": False
                },
                {
                    "stageId": "presentationscheduled",
                    "label": "Payment Details Sent",
                    "probability": 0.6,
                    "active": True,
                    "displayOrder": 2,
                    "closedWon": False
                },
                {
                    "stageId": "decisionmakerboughtin",
                    "label": "Payment Complete",
                    "probability": 0.9,
                    "active": True,
                    "displayOrder": 3,
                    "closedWon": False
                },
                {
                    "stageId": "contractsent",
                    "label": "Meet With Tutor",
                    "probability": 0.7,
                    "active": True,
                    "displayOrder": 4,
                    "closedWon": False
                },
                {
                    "stageId": "closedwon",
                    "label": "Booking Won",
                    "probability": 1,
                    "active": True,
                    "displayOrder": 5,
                    "closedWon": True
                },
                {
                    "stageId": "closedlost",
                    "label": "Booking Lost",
                    "probability": 0,
                    "active": True,
                    "displayOrder": 6,
                    "closedWon": False
                },
                {
                    "stageId": "8e33719e-4124-4510-9fd8-8e77cd334d79",
                    "label": "Booking Postponed",
                    "probability": 0.7,
                    "active": True,
                    "displayOrder": 7,
                    "closedWon": False
                },
                {
                    "stageId": "5799ff8f-c0e5-458c-b4c5-52bf4291f999",
                    "label": "No Tutor Found",
                    "probability": 0.1,
                    "active": True,
                    "displayOrder": 8,
                    "closedWon": False
                }
            ],
        }
        response = self.call_api('POST', '/deals/v1/pipelines', data=data)
        if response.status_code < 400:
            return response.json()

    def get_deal_pipeline(self, label="Client Request Pipeline"):
        response = self.call_api('GET', '/deals/v1/pipelines')
        if response.status_code < 400:
            data = [x for x in response.json() if x['label'] == label]
            if len(data) == 0:
                raise HubspotAPIException("Pipeline not created yet")
                # return self._create_deal_pipeline()
            return data[0]
        response.raise_for_status()

    def get_booking_pipeline(self, m_storage):
        result = self.get_deal_pipeline(label="Booking Pipeline")
        m_storage.booking_pipeline_id = result['pipelineId']
        m_storage.save()
        return result['pipelineId'], {x['label']: x['stageId'] for x in result['stages']}

    def update_booking_user_contact(self, vid, owing=False):
        properties = [
            single_field('hs_lead_status',
                         "OWING" if owing else 'CUSTOMER', 'property')
        ]
        data = {
            'properties': properties
        }
        new_vid = vid
        response = self.call_api(
            'POST', '/contacts/v1/contact/vid/{}/profile'.format(new_vid),
            data=data)
        if response.status_code < 400:
            return True

    def update_contact(self, request_object=None, vid=None, field="vid"):

        properties = [
        ]
        if vid:
            properties.extend([single_field('lifecyclestage', "opportunity", "property"),
                               single_field('hs_lead_status',
                                            "PENDING", 'property'),
                               ])
        if request_object:
            properties.append(
                single_field(
                    'hubspot_owner_id',
                    request_object.agent.hubspot_id, field='property')
            )
        data = {
            'properties': properties
        }
        new_vid = vid
        if request_object:
            new_vid = getattr(request_object, field)
        response = self.call_api(
            'POST', '/contacts/v1/contact/vid/{}/profile'.format(new_vid),
            data=data)
        if response.status_code < 400:
            return True

    @property
    def _contact_fields(self):
        return {
            'firstname': "first_name",
            'lastname': "last_name",
            'mobilephone': "number",
            "address": "home_address",
            "state": "state",
            "vicinity": "vicinity",
            "region": "region",
            "email": "email",
            "where_you_heard": "where_you_heard"
        }

    def get_contact(self, request_object):
        response = self.call_api(
            "GET",
            "/contacts/v1/contact/email/{}/profile".format(
                request_object.email))
        if response.status_code < 400:
            result = response.json()
            return result['vid']
        return self.create_contact(request_object)

    def _prepare_properties(self, request_object, field=None):
        fields = self._contact_fields
        lifecyclestage = {
            1: "lead",
            2: "marketingqualifiedlead",
            3: "customer",
            4: "salesqualifiedlead",
            5: "opportunity",
            6: "customer",
            7: "opportunity",
            8: "lead",
            9: "lead",
            10: "opportunity",
            11: "customer",
        }
        hs_lead_status = {
            1: "ISSUED",
            2: "COMPLETED",
            3: "PAYED",
            4: "PENDING",
            5: "REQUEST_TO_MEET",
            6: "PAYED",
            7: "COLD_REQUEST",
            8: "COLD_REQUEST",
            9: "COLD_REQUEST",
            10: "PROSPECTIVE",
            11: "PAYED",
        }
        properties = get_property_array(
            sorted(fields.items()), request_object, field=field)

        properties.extend(
            [single_field('lifecyclestage',
                          lifecyclestage[request_object.status], field=field),
             single_field('hs_lead_status',
                          hs_lead_status[request_object.status], field=field),
             single_field('hubspot_owner_id', request_object.agent.hubspot_id, field=field)]
        )
        return properties

    def create_booking_contact_properties(self, user, field=None):
        fields = {
            'firstname': "first_name",
            'lastname': "last_name",
            'mobilephone': "phone_number_details",
            "email": "email",
        }
        properties = get_property_array(
            sorted(fields.items()), user, field=field)

        properties.extend(
            [
                single_field('lifecyclestage', 'customer', field=field),
            ]
        )
        return properties

    def get_recently_created_contacts(self, bulk_callback=None):
        response = self.call_api(
            'GET', '/contacts/v1/lists/all/contacts/recent',
            params={'count': 100})
        if response.status_code < 400:
            def get_emails(pp):
                result = [x['value'] for x in pp if x['type'] == 'EMAIL']
                if len(result) > 0:
                    return result[0]
                return None
            identities = [x['identity-profiles'][0]
                          for x in response.json()['contacts']]
            new_result = [{'vid': o['vid'], 'email':get_emails(
                o['identities'])} for o in identities]
            if bulk_callback:
                bulk_callback(new_result)
            return new_result

    def get_bulk_contact_ids(self, emails, callback=None):
        params = {
            'email': [x for x in emails]
        }
        response = self.call_api('GET', '/contacts/v1/contact/emails/batch/',
                                 params=params)
        # import pdb; pdb.set_trace()
        query = [x[1]['identity-profiles'][0]
                 for x in sorted(response.json().items())]
        if response.status_code < 400:
            result = [
                {
                    'vid': x['vid'],
                    'email':[
                        y['value'] for y in x['identities']
                        if y['type'] == 'EMAIL'][0]}
                for x in query
            ]
            if callback:
                [callback(x) for x in result]
            return result
        raise HubspotAPIException("Error fetching emails")

    def create_bulk_contacts(self, request_objs, callback=None, bulk_callback=None):
        from hubspot.models import HubspotContact
        data = [{
            'email': x.email,
            'properties': self._prepare_properties(x, 'property')
        } for x in request_objs]
        response = self.call_api(
            'POST', '/contacts/v1/contact/batch/', data=data
        )
        if response.status_code < 400:
            if callback:
                list_ = [x.email for x in request_objs]
                aa = [list_[n:n + 99] for n in range(0, len(list_), 99)]
                time.sleep(5)
                for o in aa:
                    self.get_bulk_contact_ids(o, callback=callback)
            if bulk_callback:
                self.get_recently_created_contacts(bulk_callback=bulk_callback)
            return True
        return False

    def create_contact(self, request_object, booking=None):
        resultss = self._prepare_properties
        if booking:
            resultss = self.create_booking_contact_properties
        data = {
            'properties': resultss(request_object, field='property')
        }
        response = self.call_api(
            'POST', '/contacts/v1/contact/', data=data)
        result = response.json()
        vid = None
        if response.status_code not in [200, 409]:
            raise HubspotAPIException(str(response.json()))
        # import pdb; pdb.set_trace()
        if response.status_code == 200:
            vid = result['vid']
        if response.status_code == 409:
            vid = result["identityProfile"]["vid"]
            self.update_contact(vid=vid)
        return vid

    def delete_contact(self, vid):
        response = self.call_api(
            'DELETE', '/contacts/v1/contact/vid/{}'.format(vid)
        )
        if response.status_code < 400:
            return True
        return False

    def get_all_ids_and_delete(self, emails):
        result = self.get_bulk_contact_ids(emails)
        if result:
            success = []
            fields = []
            for o in result:
                y = self.delete_contact(o['vid'])
                if y:
                    success.append(True)
                else:
                    success.append(False)
                    fields.append(o['email'])
            if not all(success):
                raise HubspotAPIException(
                    "Error deleting  {}".format(", ".join(fields)))

    @property
    def _deal_fields(self):
        return {
            "time_of_lesson": "time_of_lesson",
            "request_subjects": "request_subjects",
            "days_per_week": "days_per_week",
            "class_of_child": "class_of_child",
            "id": "pk",
            "curriculum_used": "curriculum",
            "gender": "gender",
            "hours_per_day": "hours_per_day",
            "request_type": "request_type",
        }

    def get_owner_detail(self, email):
        owners = self._get_owners()
        if owners:
            result = [x for x in owners if x['email'] == email]
            if len(result) > 0:
                return result[0]['ownerId']
        raise HubspotAPIException(("could not get Agent Hubspot ID. "
                                   "Ensure that Agent is created in hubspot "
                                   "with the same email as on the site."))

    def get_dealstage(self, request):
        hs_lead_status = {
            3: "closedwon",
            4: "qualifiedtobuy",
            5: "contractsent",
            6: "closedwon",
            7: "8e33719e-4124-4510-9fd8-8e77cd334d79",
            8: "closedlost",
            10: "8e33719e-4124-4510-9fd8-8e77cd334d79",
            11: "closedwon",
        }
        try:
            result = hs_lead_status[request.status]
        except KeyError:
            result = 'appointmentscheduled'
        return result

    def get_booking_dealstage(self, booking):
        from hubspot.models import HubspotStorage
        storage = HubspotStorage.objects.first()
        pipeline, stages = self.get_booking_pipeline(storage)
        options = {
            1: "Booking Created",
            3: "Booking Closed",
            4: "Booking Cancelled",
            7: "Booking Delivered"
        }
        return pipeline, stages[options[booking.status]]

    def update_booking_deal(self, booking, field='dealId', owing=False, paid=False):
        from hubspot.models import HubspotContact
        fields = {
            'amount': "total_price"
        }
        properties = get_property_array(sorted(fields.items()), booking)
        properties.extend(self._booking_deal_stage(booking))
        if paid:
            instant = to_timestamp(
                booking.first_session) > to_timestamp(timezone.now())
            flexible = to_timestamp(booking.last_session) > to_timestamp(
                timezone.now()) > to_timestamp(booking.first_session)
            value = "End of Lesson"
            if instant:
                value = "Instant"
            if flexible:
                value = "Flexible"
            properties.extend([
                single_field('payment_schedule', value),
                single_field('createdate', getattr(booking, 'created'))
            ])
        data = {
            'properties': properties
        }
        response = self.call_api(
            "PUT", "/deals/v1/deal/{}".format(getattr(booking, field)),
            data=data)
        if response.status_code < 400:
            vid = HubspotContact.get_contact_id(booking.user.email)
            self.update_booking_user_contact(vid, owing=owing)
            return response.json()

    def update_deal(self, request_object, field='dealId'):
        data = {
            'properties': [
                single_field('dealstage', self.get_dealstage(request_object)),
                single_field('createdate', getattr(request_object, 'created')),
                single_field('closedate', getattr(request_object, 'modified')),
            ]
        }
        response = self.call_api(
            "PUT", "/deals/v1/deal/{}".format(getattr(request_object, field)),
            data=data)
        if response.status_code < 400:
            return response.json()

    def _booking_deal_stage(self, booking):
        pipeline, stage = self.get_booking_dealstage(booking)
        return [single_field('dealstage', stage),
                single_field('pipeline', pipeline),
                ]

    def create_booking_deal(self, booking, agent_id, contact_field='vid', deal_type=1, checks=lambda: None):
        from hubspot.models import HubspotContact, NoContactException
        fields = {
            'createdate': "created",
            'closedate': "last_session",
            'amount': "total_price"
        }
        dealTypes = {
            1: "newbusiness",
            2: "existingbusiness"
        }
        properties = get_property_array(sorted(fields.items()), booking)
        days_per_week = int(
            (booking.last_session - booking.first_session).days / 7)
        properties.extend([
            single_field('hubspot_owner_id', agent_id),
            single_field('dealtype', dealTypes[deal_type]),
            single_field('description', booking.tutor.email),
            single_field('days_per_week', days_per_week),
            single_field('dealname', "Booking With {} {}".format(
                booking.user.first_name, booking.user.last_name))
        ])
        properties.extend(self._booking_deal_stage(booking))
        # import pdb; pdb.set_trace()
        try:
            contact_id = HubspotContact.get_contact_id(
                booking.user.email, extra_check=checks)
        except NoContactException:
            contact_id = self.create_contact(booking.user, booking=True)
            HubspotContact.objects.create(
                email=booking.user.email, vid=contact_id)

        data = {
            "associations": {
                "associatedVids": [
                    contact_id
                ]
            },
            "properties": properties
        }

        response = self.call_api(
            'POST', '/deals/v1/deal/', data=data)
        if response.status_code < 400:
            result = response.json()
            return result['dealId']
        if response.status_code >= 400:
            raise HubspotAPIException(str(response.json()))

    def create_deal(self, request_object, contact_field='vid'):
        fields = {
            "createdate": "modified",
            "closedate": "modified",
            'dealname': "request_summary",
            'amount': "budget",
        }
        fields.update(self._deal_fields)
        properties = get_property_array(sorted(fields.items()), request_object)
        hubspot_owner_id = request_object.agent.hubspot_id
        properties.extend([
            single_field('hubspot_owner_id', hubspot_owner_id),
            single_field('dealtype', "newbusiness"),
            single_field('dealstage', self.get_dealstage(request_object)),
            single_field('pipeline', "default"),
            single_field('description', "{} {}".format(
                getattr(request_object, 'expectation'), ", ".join(request_object.available_days)))
        ])
        data = {
            "associations": {
                "associatedVids": [
                    getattr(request_object, contact_field)
                ]
            },
            "properties": properties
        }

        response = self.call_api(
            'POST', '/deals/v1/deal/', data=data)
        if response.status_code < 400:
            result = response.json()
            return result['dealId']
        if response.status_code >= 400:
            raise HubspotAPIException(str(response.json()))

    def _create_deal_group(self, name):
        data = {
            "name": name,
            "displayName": "Lesson Information Details",
            "properties": initialize_deal_property_array(self._deal_fields)
        }
        response = self.call_api(
            'POST', "/properties/v1/deals/groups/", data=data)
        if response.status_code < 400:
            return response.json()

    def get_lesson_deal_group(self, name):
        response = self.call_api(
            'GET', "/properties/v1/deals/groups/named/{}".format(name),
            params={
                "includeProperties": True
            }
        )
        if response.status_code < 400:
            return response.json()
        return self._create_deal_group(name)

    def _call_engagement(self, data):
        response = self.call_api('POST', '/engagements/v1/engagements',
                                 data=data)
        if response.status_code < 400:
            return response.json()

    def send_email(self, request_object, deal='dealId', agent_field='first_name'):
        template_data = request_object.get_email_template()
        agent = request_object.agent
        data = {
            "engagement": {
                "active": True,
                "ownerId": agent.hubspot_id,
                "type": "EMAIL",
            },
            "associations": {
                "dealIds": [getattr(request_object, deal)],
            },
            "metadata": {
                "from": {
                    "email": agent.email,
                    "firstName": getattr(agent, agent_field),
                },
                "to": [
                    {
                        "email": request_object.email
                    }
                ],
                "cc": [],
                "bcc": [],
                "subject": template_data['subject'],
                "html": template_data['html'],
                "text": template_data['text']
            }
        }
        return self._call_engagement(data)

    def send_profile_to_agent(self, agent_id, dealId, message_body):
        data = {
            "engagement": {
                "active": True,
                "ownerId": agent_id,
                "type": "NOTE",
            },
            "associations": {
                "dealIds": [dealId],
            },
            "metadata": {
                "body": message_body
            }
        }
        return self._call_engagement(data)

    def follow_up_contacts(self, agent_id, dealIds, currentDate):
        data = {
            "engagement": {
                "active": True,
                "ownerId": agent_id,
                "type": "TASK",
            },
            "associations": {
                "dealIds": dealIds,
            },
            "metadata": {
                "body": "Client yet to open email after 2 days. Follow up on client.",
                "subject": "FOLLOWUP Deal",
                "status": "NOT_STARTED",
                "taskType": "CALL",
                "reminders": [
                    to_timestamp(currentDate)

                ],
                "sendDefaultReminder": True
            }
        }
        return self._call_engagement(data)

    def initialize_hubspot(self):
        """
        Create all the properties for contacts,
        deal pipelines, deals, deal_groups, lifecyclestages,
        lead_status.
        It also fetches all the agents from hubspot and saves them in
        the db.
        """
        from .models import HubspotStorage
        m_instance = HubspotStorage.get_token()
        if not m_instance:
            m_instance = HubspotStorage.objects.create()
        self._get_or_create_contact_properties(m_instance)
        self.fetch_owners(m_instance)
        self._get_or_create_deal_properties(m_instance)

    def _create_contact_property(self, name):
        data = {
            "name": name,
            "label": name,
            "groupName": "contactinformation",
            "type": "string",
            "fieldType": "text",
            "formField": True,
        }
        result = self.call_api(
            "POST", "/properties/v1/contacts/properties", data=data)

        if result.status_code in [200, 409]:

            return True
        return False

    def _create_deal_property(self, name, groupname):
        data = {
            "name": name,
            "label": name,
            "description": "",
            "groupName": groupname,
            "type": "string",
            "fieldType": "text",
            "formField": True
        }
        result = self.call_api(
            "POST",  "/properties/v1/deals/properties/", data=data)

        if result.status_code in [200, 409]:

            return True
        return False

    def bulk_check_for_fields_creation(self, result, condition_callback):
        if len(result) > 0:
            success = []
            fields = []
            for o in sorted(result):
                y = condition_callback(o)
                if y:
                    success.append(True)
                else:
                    success.append(False)
                    fields.append(o)
            if not all(success):
                raise HubspotAPIException(
                    "Error occured when adding {}".format(", ".join(fields)))

    def create_passed_properties(self, fields_to_check):
        response = self.call_api(
            "GET", "/properties/v1/contacts/properties")
        if response.status_code < 400:
            names = [x['name'] for x in response.json()]
            result = [x for x in fields_to_check if x not in names]
            self.bulk_check_for_fields_creation(
                result, lambda x: self._create_contact_property(x))
            m_instance.contact_properties = True
            m_instance.save()

    def _get_or_create_contact_properties(self, m_instance):
        if not m_instance.contact_properties:
            fields_to_check = ['firstname', 'lastname', 'mobilephone', 'address',
                               'state', 'vicinity', 'region', 'email', 'where_you_heard']
            response = self.call_api(
                "GET", "/properties/v1/contacts/properties")
            if response.status_code < 400:
                names = [x['name'] for x in response.json()]
                result = [x for x in fields_to_check if x not in names]
                if len(result) > 0:
                    success = []
                    fields = []
                    for o in sorted(result):
                        y = self._create_contact_property(o)
                        if y:
                            success.append(True)
                        else:
                            success.append(False)
                            fields.append(o)
                    if not all(success):
                        raise HubspotAPIException(
                            "Error occured when adding {}".format(", ".join(fields)))
                m_instance.contact_properties = True
                m_instance.save()

    def fetch_owners(self, m_instance, queryset=None):
        from .models import HubspotOwner
        result = self._get_owners()
        if not m_instance.fetched_agents:
            exists = HubspotOwner.objects.values_list('email', flat=True)
            if result:
                alls = [HubspotOwner(email=x['email'],
                                     hubspot_id=x['ownerId']) for x in result if x['email'] not in exists]
                HubspotOwner.objects.bulk_create(alls)
                m_instance.fetched_agents = True
                m_instance.save()
        query = queryset
        if not query:
            query = HubspotOwner.objects.all()
        if query:
            for x in query:
                try:
                    recored = [u for u in result if u['email'] == x.email][0]
                    x.hubspot_id = recored['ownerId']
                    x.save()
                except IndexError:
                    x.delete()

    def _get_or_create_deal_properties(self, m_instance):
        fields = []
        if m_instance.deal_group_name and not m_instance.deal_properties:
            response = self.get_lesson_deal_group(m_instance.deal_group_name)
            if not response:
                raise HubspotAPIException(
                    "Deal Group {} couldn't be created. ".format(m_instance.deal_group_name))
            # import pdb; pdb.set_trace()
            properties = response.get('properties') or []
            result = [x['name'] for x in properties]
            missing_keys = [x for x in self._deal_fields.keys()
                            if x not in result]
            self.bulk_check_for_fields_creation(missing_keys, lambda x: self._create_deal_property(
                x, m_instance.deal_group_name
            ))
            m_instance.deal_properties = True
            m_instance.save()

    def _delete_contact_property(self, field):
        response = self.call_api(
            'DELETE', '/properties/v1/contacts/properties/named/{}'.format(field))
        if response.status_code < 400:
            return True
        return False

    def _delete_deal_group(self, name):
        response = self.call_api(
            'DELETE', '/properties/v1/deals/groups/named/{}'.format(name)
        )
        if response.status_code < 400:
            return True
        return False
