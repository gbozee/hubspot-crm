from builtins import super
try:
    from unittest import mock
except ImportError:
    import mock
from django.shortcuts import reverse
from hubspot.signals import hubspot_action_after_message_webhook
from django.dispatch import receiver
from django.test import TestCase


@receiver(hubspot_action_after_message_webhook)
def signal_called(sender, **kwargs):
    data = kwargs.get('params')
    generic_function(**data)


def generic_function(**params):
    print(params)


class EmailWebhookTestCase(TestCase):
    def setUp(self):
        self.patcher = mock.patch(
            'test_crm.tests.test_signals.generic_function')
        self.mocker = self.patcher.start()

    def tearDown(self):
        self.patcher.stop()

    def test_signal_called_without_issues(self):
        url = reverse('hubspot:mailgun_open')
        response = self.client.post(url, data={
            'event': 'open',
            'recipient': 'Biola',
            'timestamp': 2007,
            'tag': "fhello_world"
        })
        self.mocker.assert_called_once_with(
            event='open', recipient='Biola', timestamp='2007',
            tag='fhello_world'
        )
        self.assertEqual(response.json()['recipient'], "Biola")
