# -*- coding: utf-8 -*-
from __future__ import unicode_literals 
from builtins import super,str

from django.shortcuts import reverse
from hubspot import HubspotAPI, HubspotAPIException, initialize_deal_property_array
from hubspot.models import HubspotStorage, HubspotOwner
import datetime
from . import BaseTestCase, mock, TestCase


class ContactPropertyTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        self.inst = HubspotStorage.objects.create()
        self.path = "{}/properties/v1/contacts/properties".format(
            self.instance.call_url)

    def test_all_fields_already_exist(self):
        self.assertFalse(self.inst.contact_properties)

        self.mock_get.return_value = self.mock_response(
            [{'name': x} for x in ['firstname', 'lastname', 'mobilephone',
                                   'state', 'vicinity', 'region', 'address', 'email', 'where_you_heard']], status_code=200, overwrite=True)
        self.instance._get_or_create_contact_properties(self.inst)
        self.get_assert()
        self.mock_post.assert_not_called()
        self.assertTrue(self.inst.contact_properties)

    def get_assert(self):
        self.mock_get.assert_called_once_with(self.path, params={
            'hapikey': self.instance.api_key,
        },
            headers={'content-type': 'application/json'}
        )

    def post_assert(self, not_found):
        self.mock_post.assert_has_calls([
            mock.call(self.path, json={
                'name': x,
                "label": x,
                "groupName": "contactinformation",
                "type": "string",
                "fieldType": "text",
                "formField": True,
            }, params={
                'hapikey': self.instance.api_key,
            }, headers={'content-type': "application/json"}) for x in not_found])

    def test_some_fields_already_exist(self):
        self.assertFalse(self.inst.contact_properties)
        not_found = ['vicinity', 'region']
        found = ['firstname', 'lastname', 'mobilephone',
                 'state',  'address', 'email', 'where_you_heard']
        self.mock_get.return_value = self.mock_response(
            [{'name': x} for x in found], status_code=200, overwrite=True)
        self.mock_post.return_value = self.mock_response(
            {'name': "xxx"}
        )
        self.instance._get_or_create_contact_properties(self.inst)
        self.get_assert()
        self.post_assert(sorted(not_found))
        self.assertTrue(self.inst.contact_properties)

    def test_when_error_occurs_on_post(self):
        found = ['firstname', 'lastname', 'mobilephone',
                 'state',  'address', 'email', 'where_you_heard']
        not_found = ['vicinity', 'region']
        self.assertFalse(self.inst.contact_properties)

        self.mock_post.return_value = self.mock_response(
            {'name': "xxx"}, status_code=400
        )
        self.mock_get.return_value = self.mock_response(
            [{'name': x} for x in found], status_code=200, overwrite=True)

        with self.assertRaises(HubspotAPIException) as context:
            self.instance._get_or_create_contact_properties(self.inst)
            self.get_assert()

        self.assertEqual(
            'Error occured when adding region, vicinity'.replace("AssertError: ",""), str(context.exception))
        self.assertFalse(self.inst.contact_properties)

    def test_not_called_when_already_true(self):
        self.inst.contact_properties = True
        self.inst.save()
        self.mock_get.assert_not_called()
        self.mock_post.assert_not_called()

    def delete_assert(self, status):
        self.mock_delete.return_value = self.mock_response(
            {}, status_code=status, overwrite=True
        )
        result = self.instance._delete_contact_property('region')
        self.mock_delete.assert_called_once_with("{}/properties/v1/contacts/properties/named/{}".format(
            self.instance.call_url, 'region',),
            params={
                'hapikey': self.instance.api_key,
        },
            headers={
            'content-type': "application/json"
        }
        )
        return result

    def test_can_delete_contact_property(self):
        result = self.delete_assert(200)
        self.assertTrue(result)

    def test_delete_on_400(self):
        result = self.delete_assert(400)
        self.assertFalse(result)


class FetchOwnersTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        self.inst = HubspotStorage.objects.create()
        self.path = "{}/owners/v2/owners/".format(
            self.instance.call_url)

    def test_on_agents_added(self):
        self.assertFalse(self.inst.fetched_agents)
        self.assertEqual(HubspotOwner.objects.count(), 0)
        self.mock_get.return_value = self.mock_response(
            [
                {
                    "ownerId": 64,
                    "email": "holly@dundermifflin.com",

                }, {
                    "ownerId": 65,
                    "email": "jjj@example.com"
                }],
            status_code=200, overwrite=True)
        self.instance.fetch_owners(self.inst)
        self.get_assert()
        self.assertTrue(self.inst.fetched_agents)
        self.assertEqual(HubspotOwner.objects.count(), 2)
        first = HubspotOwner.objects.first()
        self.assertEqual(first.hubspot_id, 64)
        self.assertEqual(first.email, "holly@dundermifflin.com")

    def get_assert(self):
        self.mock_get.assert_called_once_with(self.path, params={
            'hapikey': self.instance.api_key,
        },
            headers={'content-type': 'application/json'}
        )

    def test_when_error_occured(self):
        self.assertFalse(self.inst.fetched_agents)
        self.assertEqual(HubspotOwner.objects.count(), 0)
        self.mock_get.return_value = self.mock_response(
            {'error': "Yes"},
            status_code=400, overwrite=True)
        self.instance.fetch_owners(self.inst)
        self.assertFalse(self.inst.fetched_agents)
        self.assertEqual(HubspotOwner.objects.count(), 0)

    def test_owners_can_be_refetched(self):
        HubspotOwner.objects.bulk_create([
            HubspotOwner(email="holly@dundermifflin.com",hubspot_id=1),
            HubspotOwner(email="jj@example.com",hubspot_id=2)
        ])
        self.assertEqual(HubspotOwner.objects.count(), 2)
        self.mock_get.return_value = self.mock_response(
            [
                {
                    "ownerId": 2,
                    'email': "jj@example.com"
                },

                {
                    "ownerId": 64,
                    "email": "holly@dundermifflin.com",

                }, {
                    "ownerId": 65,
                    "email": "jjj@example.com"
                }],
            status_code=200, overwrite=True)
        self.instance.fetch_owners(self.inst)
        self.get_assert()
        self.assertEqual(HubspotOwner.objects.count(), 3)
        first = HubspotOwner.objects.get(email="holly@dundermifflin.com")
        self.assertEqual(first.hubspot_id, 64)

class DealPropertiesTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        self.inst = HubspotStorage.objects.create()
        self.b_path = "{}/properties/v1/deals".format(self.instance.call_url)
        self.path = "{}/groups/".format(
            self.b_path)
        # self.mock_post2 = self.patcher.start()

    def test_when_deal_group_already_exists(self):
        self.mock_get.return_value = self.mock_response(
            {
                "name": self.inst.deal_group_name,
                'properties': [{'name': x} for x in self.instance._deal_fields.keys()]
            }, status_code=200, overwrite=True
        )
        self.assertFalse(self.inst.deal_properties)
        self.instance._get_or_create_deal_properties(self.inst)
        self.get_assert()
        self.mock_post.assert_not_called()
        self.assertTrue(self.inst.deal_properties)

    def test_group_exists_but_not_all_fields_exists(self):
        found = ['time_of_lesson', 'request_subjects', 'days_per_week',
                 'class_of_child',  'id', 'curriculum_used', 'gender']
        not_found = ['hours_per_day', 'request_type']
        self.assertFalse(self.inst.deal_properties)
        self.mock_get.return_value = self.mock_response(
            {'name': self.inst.deal_group_name,
             'properties': [{'name': x} for x in found]}, status_code=200, overwrite=True)
        self.mock_post.return_value = self.mock_response(
            {'name': "xxx"}
        )
        self.instance._get_or_create_deal_properties(self.inst)
        self.get_assert()
        self.post_assert(not_found)
        self.assertTrue(self.inst.deal_properties)

    def test_when_deal_group_doesnt_exist_but_was_created(self):
        self.mock_get.return_value = self.mock_response(
            {
                "status": "error",
                "message": "Couldn't find a Group with the given name 'lesson_information_details'.",
                "correlationId": "1ef0f7eb-9b3e-4024-a30e-b24a50b0df71",
                "requestId": "aee34ec5efd6b4547756b31691deb1d2"
            }, status_code=400, overwrite=True
        )
        self.mock_post.return_value = self.mock_response(
            {
                "name": self.inst.deal_group_name,
                "displayName": "Lesson Information Details",
                "properties": [
                ]
            },
            status_code=200, overwrite=True
        )
        self.assertFalse(self.inst.deal_properties)
        self.instance._get_or_create_deal_properties(self.inst)
        self.mock_post.assert_any_call(
            self.path, json={
                "name": self.inst.deal_group_name,
                "displayName": "Lesson Information Details",
                "properties": initialize_deal_property_array(self.instance._deal_fields)
            },
            params={
                'hapikey': self.instance.api_key
            },
            headers={
                'content-type': "application/json"
            }
        )
        self.post_assert(sorted(self.instance._deal_fields.keys()))
        self.assertTrue(self.inst.deal_properties)

    def test_when_deal_group_doesnt_exist_but_couldnt_be_created(self):
        self.mock_get.return_value = self.mock_response(
            {
                "status": "error",
                "message": "Couldn't find a Group with the given name 'lesson_information_details'.",
                "correlationId": "1ef0f7eb-9b3e-4024-a30e-b24a50b0df71",
                "requestId": "aee34ec5efd6b4547756b31691deb1d2"
            }, status_code=400, overwrite=True
        )
        self.mock_post.return_value = self.mock_response(
            {
                "status": "error",
                "message": "Couldn't find a Group with the given name 'lesson_information_details'.",
                "correlationId": "1ef0f7eb-9b3e-4024-a30e-b24a50b0df71",
                "requestId": "aee34ec5efd6b4547756b31691deb1d2"
            }, status_code=400, overwrite=True
        )
        self.assertFalse(self.inst.deal_properties)
        with self.assertRaises(HubspotAPIException) as context:
            self.instance._get_or_create_deal_properties(self.inst)
            self.get_assert()
            self.mock_post.assert_called_once_with(
                self.path, json={
                    "name": self.inst.deal_group_name,
                    "displayName": "Lesson Information Details",
                    "properties": initialize_deal_property_array(self.instance._deal_fields)
                },
                params={
                    'hapikey': self.instance.api_key
                },
                headers={
                    'content-type': "application/json"
                }
            )
        self.assertEqual(
            "Deal Group {} couldn't be created. ".format(self.inst.deal_group_name), str(context.exception))

        self.assertFalse(self.inst.deal_properties)

    def delete_assert(self, status):
        self.mock_delete.return_value = self.mock_response(
            {}, status_code=status, overwrite=True
        )
        result = self.instance._delete_deal_group('region')
        self.mock_delete.assert_called_once_with("{}/properties/v1/deals/groups/named/{}".format(
            self.instance.call_url, 'region',),
            params={
                'hapikey': self.instance.api_key,
        },
            headers={
            'content-type': "application/json"
        }
        )
        return result

    def test_can_delete_contact_property(self):
        result = self.delete_assert(200)
        self.assertTrue(result)

    def test_delete_on_400(self):
        result = self.delete_assert(400)
        self.assertFalse(result)

    def get_assert(self):
        self.mock_get.assert_called_once_with("{}named/{}".format(
            self.path, self.inst.deal_group_name),
            params={
                'includeProperties': True,
                'hapikey': self.instance.api_key
        },
            headers={
                'content-type': "application/json"
        }
        )

    def post_assert(self, not_found):
        calls = [
            mock.call("{}/properties/".format(self.b_path), json={
                'name': x,
                "label": x,
                "description":"",
                "groupName": self.inst.deal_group_name,
                "type": "string",
                "fieldType": "text",
                "formField": True,
            }, params={
                'hapikey': self.instance.api_key,
            }, headers={'content-type': "application/json"}) for x in not_found]
        self.mock_post.assert_has_calls(calls)

    def test_some_fields_already_exist(self):
        self.assertFalse(self.inst.deal_properties)
        found = ['time_of_lesson', 'request_subjects', 'days_per_week',
                 'class_of_child',  'id', 'curriculum_used', 'gender']
        not_found = ['hours_per_day', 'request_type']
        self.mock_get.return_value = self.mock_response(
            {'name': self.inst.deal_group_name,
             'properties': [{'name': x} for x in found]}, status_code=200, overwrite=True)
        self.mock_post.return_value = self.mock_response(
            {'name': "xxx"}
        )
        self.instance._get_or_create_deal_properties(self.inst)
        self.get_assert()
        self.post_assert(sorted(not_found))
        self.assertTrue(self.inst.deal_properties)

    def test_when_error_occurs_on_post(self):
        found = ['time_of_lesson', 'request_subjects', 'days_per_week',
                 'class_of_child',  'id', 'curriculum_used', 'gender']
        not_found = ['hours_per_day', 'request_type']
        self.assertFalse(self.inst.deal_properties)

        self.mock_post.return_value = self.mock_response(
            {'name': "xxx"}, status_code=400
        )

        self.mock_get.return_value = self.mock_response(
            {"name": self.inst.deal_group_name,
             "displayName": "Lesson Information Details",
                "properties": [{'name': x} for x in found
                               ]}, status_code=200, overwrite=True
        )
        with self.assertRaises(HubspotAPIException) as context:
            self.instance._get_or_create_deal_properties(self.inst)
            self.get_assert()

        self.assertEqual(
            'Error occured when adding hours_per_day, request_type', str(context.exception))
        self.assertFalse(self.inst.deal_properties)

    def test_not_called_when_already_true(self):
        self.inst.deal_properties = True
        self.inst.save()
        self.mock_get.assert_not_called()
        self.mock_post.assert_not_called()


class UsageTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        self.instance._delete_deal_group('lesson_information_details')
        
    
    def test_initialize_hubspot(self):
        self.assertEqual(HubspotStorage.objects.count(), 0)
        self.assertEqual(HubspotOwner.objects.count(), 0)
        self.instance.initialize_hubspot()
        self.assertEqual(HubspotStorage.objects.count(), 1)
        inst = HubspotStorage.objects.first()
        self.assertTrue(inst.contact_properties)
        self.assertTrue(inst.fetched_agents)
        self.assertTrue(inst.deal_properties)
        self.assertEqual(HubspotOwner.objects.count(), 7)
