# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
from builtins import super, str, int
from decimal import Decimal
from django.shortcuts import reverse
from hubspot import (HubspotAPI, HubspotAPIException, get_property_array,
                     to_timestamp)
from hubspot.models import HubspotStorage, HubspotOwner, HubspotContact
import datetime
from . import (
    BaseTestCase, mock, TestCase, BaseRequestTutor,
    Agent)
import pytz


class TestContactModelTestCase(TestCase):
    def test_new_record(self):
        instance = BaseRequestTutor(
            email='j@example.com',vid=3000
        )
        self.assertEqual(HubspotContact.objects.count(), 0)
        result = HubspotContact.get_contact_id(instance.email, lambda: instance.vid)
        self.assertEqual(HubspotContact.objects.count(), 1)
        self.assertEqual(result, 3000)

    def test_existing_record(self):
        instance = HubspotContact.objects.create(
            email='j@example.com', vid=3000)
        result = HubspotContact.get_contact_id(
            instance.email, lambda: instance.vid)
        self.assertEqual(HubspotContact.objects.count(), 1)
        self.assertEqual(result, 3000)


    def test_bulk_record_update_with_new_data(self):
        instance = BaseRequestTutor(
            email='j@example.com', vid=3000
        )
        instance2 = BaseRequestTutor(email='l@example.com',vid=4000)
        self.assertEqual(HubspotContact.objects.count(), 0)
        HubspotContact.bulk_update_record([instance,instance2])
        self.assertEqual(HubspotContact.objects.count(), 2)

    def test_bulk_record_update_with_existing_data(self):
        instance = HubspotContact.objects.create(
            email='j@example.com', vid=3000)
        self.assertEqual(HubspotContact.objects.count(), 1)
        instance = BaseRequestTutor(
            email='j@example.com', vid=3000
        )
        instance2 = BaseRequestTutor(email='l@example.com', vid=4000)
        HubspotContact.bulk_update_record([instance, instance2])
        self.assertEqual(HubspotContact.objects.count(), 2)
