# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from builtins import super, str
from django.test import TestCase
try:
    from unittest import mock
except ImportError:
    import mock
from hubspot.models import HubspotOwner, HubspotContact


class MockRequest:

    def __init__(self, response, **kwargs):
        self.response = response
        self.overwrite = False
        if kwargs.get('overwrite'):
            self.overwrite = True
        self.status_code = kwargs.get('status_code', 200)

    @classmethod
    def raise_for_status(cls):
        pass

    def json(self):
        if self.overwrite:
            return self.response
        return {'data': self.response}


class BaseTestCase(TestCase):
    def setUp(self):
        self.patcher = mock.patch('hubspot.requests.post')
        self.get_patcher = mock.patch('hubspot.requests.get')
        self.put_patcher = mock.patch('hubspot.requests.put')
        self.delete_patcher = mock.patch('hubspot.requests.delete')
        self.mock_post = self.patcher.start()
        self.mock_get = self.get_patcher.start()
        self.mock_put = self.put_patcher.start()
        self.mock_delete = self.delete_patcher.start()

    def tearDown(self):
        self.patcher.stop()
        self.get_patcher.stop()
        self.put_patcher.stop()
        self.delete_patcher.stop()

    def mock_response(self, data, **kwargs):
        return MockRequest(data, **kwargs)

    def initialize_mock(self, status=200, **kwargs):
        if status == 200:
            solution = {
                "access_token": "xxxx",
                "refresh_token": "yyyyyyyy-yyyy-yyyy-yyyy-yyyyyyyyyyyy",
                "expires_in": 21600
            }
        else:
            solution = {
                "error": "error_code",
                "error_description": "A human readable error message"
            }
        solution.update(**kwargs)
        self.mock_post.return_value = self.mock_response(
            solution, status_code=status, overwrite=True)
        return solution


class Agent(object):
    def __init__(self, **kwargs):
        for o in ["email",'first_name']:
            setattr(self, o, kwargs.get(o))

    @property
    def hubspot_id(self):
        response = HubspotOwner.objects.filter(email=self.email).first()
        if response:
            return response.hubspot_id
        return None

class NewUser(object):
    vid = None
    def __init__(self, **kwargs):
        for o in kwargs.keys():
            setattr(self, o, kwargs.get(o))

    @property
    def hubspot_id(self):
        result = HubspotContact.get_contact_id(self.email,
        lambda: self)
        return result



class Booking(object):
    INITIALIZED = 0
    SCHEDULED = 1
    PENDING = 2
    COMPLETED = 3
    CANCELLED = 4
    BANK_TRANSFER = 6
    DELIVERED = 7
    dealId = None
    def __init__(self,**kwargs):
        for o in kwargs.keys():
            setattr(self, o, kwargs.get(o))

    def create_new_deal(self, instance, agent, deal_type=1):
        self.dealId = instance.create_booking_deal(self, agent.hubspot_id,deal_type=deal_type)


class BaseRequestTutor(object):
    dealId = None
    vid = None

    def __init__(self, **kwargs):
        for o in kwargs.keys():
            setattr(self, o, kwargs.get(o))

    def save_vid(self, instance):
        self.vid = instance.create_contact(self)

    @property
    def request_summary(self):
        return "{} subjects requested".format(", ".join(self.request_subjects))

    def create_new_deal(self, instance):
        self.dealId = instance.create_deal(self)

    @property
    def deal_description(self):
        return "{} {}".format(self.expectation, ", ".join(self.available_days))

    def message_to_client(self):
        return "found tutors's url: {}\nadmin_url for list of tutors: {}".format("google.com", "facebook.com")

    def get_email_template(self):
        return {
            'subject': "Hello World",
            'html': "<h1>Hello world</h1>",
            'text': "Hello world",
        }
