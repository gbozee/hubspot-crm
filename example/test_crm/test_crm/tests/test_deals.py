# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
from builtins import super, str, int
from decimal import Decimal
from django.shortcuts import reverse
from hubspot import (HubspotAPI, HubspotAPIException, get_property_array,
                     to_timestamp, as_timestamp)
from hubspot.models import HubspotStorage, HubspotOwner, HubspotContact
import datetime
from . import (
    BaseTestCase, mock, TestCase, BaseRequestTutor, Booking,
    NewUser,
    Agent)
import pytz


class SharedMixin(object):
    def create_booking(self):
        user1 = BaseRequestTutor()
        self.booking = Booking(
            user=NewUser(email="j@example.com", vid=32342),
            total_price=7000,
            status=1,
            created=datetime.datetime(2017, 8, 4, 11, 12)
        )

    def create_user(self, vid=None, agent=None):
        self.agent = agent
        self.instance.initialize_hubspot()
        self.user = BaseRequestTutor(
            first_name="James", last_name="Novac",
            number="+2347035209976", home_address="20 irabor", state="Lagos",
            email="oo@example.com", status=1,
            vicinity="ibadan",
            where_you_heard="from google",
            region='',
            created=datetime.datetime(2017, 10, 3, 12, 11, 2, tzinfo=pytz.utc),
            modified=datetime.datetime(
                2017, 10, 5, 12, 11, 2, tzinfo=pytz.utc),
            budget=Decimal(50000),
            request_subjects=["Mathematics", "English"],
            vid=vid,
            expectation="I want my child to succed.",
            time_of_lesson="5:30pm",
            days_per_week=4,
            pk=33,
            class_of_child="Adult",
            curriculum="",
            gender="M",
            hours_per_day=7,
            available_days=["Monday", "Tuesday"],
            request_type="parent",
            agent=self.agent)


class DealCreationTestCase(SharedMixin, BaseTestCase):

    def setUp(self):
        super().setUp()
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        self.inst = HubspotStorage.objects.create(
            contact_properties=True, fetched_agents=True,
            deal_properties=True
        )
        HubspotOwner.objects.create(email="hb@tuteria.com", hubspot_id=222)

        agent = Agent(email="hb@tuteria.com", first_name="James Bond")
        self.create_user(24136, agent=agent)


    def test_create_deal(self):
        self.assertIsNone(self.user.dealId)
        self.mock_post.return_value = self.mock_response(
            {
                "portalId": self.user.dealId,
                "dealId": 151088,
                "isDeleted": False,
                "associations": {
                    "associatedVids": [
                        self.user.vid
                    ],

                }, }, statuss_code=200, overwrite=True
        )
        self.user.create_new_deal(self.instance)
        self.mock_post.assert_called_once_with(
            "{}/deals/v1/deal/".format(self.instance.call_url),
            json={
                'associations': {
                    "associatedVids": [
                        24136
                    ]
                },
                'properties': [
                    get_value('amount', "50000"),
                    get_value('class_of_child', "Adult"),
                    get_value('closedate', int(1507205462000)),
                    get_value('createdate', int(1507205462000)),
                    get_value("curriculum_used", ""),
                    get_value("days_per_week", "4 weeks"),
                    get_value("dealname",
                              "Mathematics, English subjects requested", ),
                    get_value("gender", "M"),
                    get_value("hours_per_day", "7"),
                    get_value("id", "33"),
                    get_value('request_subjects', "Mathematics, English"),
                    get_value("request_type", "parent"),
                    get_value('time_of_lesson', "5:30pm"),
                    get_value('hubspot_owner_id', "222"),
                    get_value("dealtype", "newbusiness"),
                    get_value("dealstage", "appointmentscheduled"),
                    get_value('pipeline', "default"),
                    get_value('description',
                              "I want my child to succed. Monday, Tuesday"),
                ]

            },
            params={
                'hapikey': self.instance.api_key
            },
            headers={
                'content-type': 'application/json'
            }
        )
        self.assertEqual(self.user.dealId, 151088)

    def test_profile_information_to_client(self):
        self.user.dealId = 151088
        self.mock_post.return_value = self.mock_response(
            {"engagement": {
                "id": 328550660,
            },
                "associations": {
                "contactIds": [
                    self.user.vid
                ],
            },
                "metadata": {
                "body": self.user.message_to_client()
            }
            },
            status_code=200,
            overwrite=True
        )
        self.instance.send_profile_to_agent(
            self.user.agent.hubspot_id,
            self.user.dealId, self.user.message_to_client())

        self.mock_post.assert_called_once_with(
            "{}/engagements/v1/engagements".format(self.instance.call_url), json={
                "engagement": {
                    "active": True,
                    "ownerId": self.agent.hubspot_id,
                    "type": "NOTE",
                },
                "associations": {
                    "dealIds": [151088],
                },
                "metadata": {
                    "body": self.user.message_to_client()
                }
            },
            headers={
                'content-type': "application/json"
            },
            params={
                'hapikey': self.instance.api_key
            }
        )

    def test_email_sent_to_hubspot(self):
        self.user.dealId = 151088
        self.mock_post.return_value = self.mock_response(
            {"engagement": {
                "id": 328550660,
            },
                "associations": {
                "contactIds": [
                    self.user.vid
                ],
            },
                "metadata": {
                "body": "Client yet to open email after 2 days. Follow up on client."
            }
            },
            status_code=200,
            overwrite=True
        )
        self.instance.send_email(
            self.user, deal="dealId")
        template_data = self.user.get_email_template()
        self.mock_post.assert_called_once_with(
            "{}/engagements/v1/engagements".format(
                self.instance.call_url), json={
                "engagement": {
                    "active": True,
                    "ownerId": self.agent.hubspot_id,
                    "type": "EMAIL",
                },
                "associations": {
                    "dealIds": [151088],
                },
                "metadata": {
                    "from": {
                        "email": self.agent.email,
                        "firstName": self.agent.first_name,
                    },
                    "to": [
                        {
                            "email": self.user.email
                        }
                    ],
                    "cc": [],
                    "bcc": [],
                    "subject": template_data['subject'],
                    "html": template_data['html'],
                    "text": template_data['text']
                }
            },
            headers={
                'content-type': "application/json"
            },
            params={
                'hapikey': self.instance.api_key
            }
        )

    def test_reminder_sent_to_contact_about_deal(self):
        self.user.dealId = 151088
        self.mock_post.return_value = self.mock_response(
            {"engagement": {
                "id": 328550660,
            },
                "associations": {
                "contactIds": [
                    self.user.vid
                ],
            },
                "metadata": {
                "body": "Client yet to open email after 2 days. Follow up on client."
            }
            },
            status_code=200,
            overwrite=True
        )
        dt = datetime.datetime(2017, 10, 15, 1, tzinfo=pytz.utc)
        self.instance.follow_up_contacts(
            self.user.agent.hubspot_id,
            [self.user.dealId], dt)
        self.mock_post.assert_called_once_with(
            "{}/engagements/v1/engagements".format(self.instance.call_url), json={
                "engagement": {
                    "active": True,
                    "ownerId": self.agent.hubspot_id,
                    "type": "TASK",
                },
                "associations": {
                    "dealIds": [151088],
                },
                "metadata": {
                    "body": "Client yet to open email after 2 days. Follow up on client.",
                    "subject": "FOLLOWUP Deal",
                    "status": "NOT_STARTED",
                    "taskType": "CALL",
                    "reminders": [
                        to_timestamp(dt)
                    ],
                    "sendDefaultReminder": True
                }
            },
            headers={
                'content-type': "application/json"
            },
            params={
                'hapikey': self.instance.api_key
            }
        )


class ActualUsageTestCase(SharedMixin, TestCase):
    def setUp(self):
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        agent = Agent(email="biola@tuteria.com", first_name="Abiola Oyeniyi")
        self.create_user(agent=agent)
        self.instance.get_all_ids_and_delete(
            [self.user.email])

    def test_create_deal(self):
        self.assertTrue(HubspotOwner.objects.count() > 0)
        self.assertIsNotNone(self.user.agent.hubspot_id)
        self.assertIsNone(self.user.dealId)
        self.assertIsNone(self.user.vid)
        self.user.save_vid(self.instance)
        self.assertIsNotNone(self.user.vid)
        self.user.create_new_deal(self.instance)
        self.assertIsNotNone(self.user.dealId)
        response = self.instance.send_profile_to_agent(
            self.agent.hubspot_id, self.user.dealId, self.user.message_to_client()
        )
        self.assertIsNotNone(response)
        rr = self.instance.follow_up_contacts(self.user.agent.hubspot_id,
                                              [self.user.dealId], datetime.datetime(2017, 10, 15, tzinfo=pytz.utc))
        self.assertIsNotNone(rr)
        rro = self.instance.send_email(self.user)
        self.assertIsNotNone(rro)
        result = self.instance.update_deal(self.user)
        self.assertIsNotNone(result)

    def test_update_deal(self):
        pass


def get_value(key, value):
    return {
        'name': key,
        'value': value}
