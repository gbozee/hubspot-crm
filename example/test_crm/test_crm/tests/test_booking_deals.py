# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
from builtins import super, str, int
from decimal import Decimal
from django.shortcuts import reverse
from hubspot import (HubspotAPI, HubspotAPIException, get_property_array,
                     to_timestamp, as_timestamp)
from hubspot.models import HubspotStorage, HubspotOwner, HubspotContact
import datetime
from . import (
    BaseTestCase, mock, TestCase, BaseRequestTutor, Booking,
    NewUser,
    Agent)
import pytz


class BookingPipelineTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        self.inst = HubspotStorage.objects.create(
            contact_properties=True, fetched_agents=True,
            deal_properties=True
        )
        HubspotOwner.objects.create(email="hb@tuteria.com", hubspot_id=222)

        agent = Agent(email="hb@tuteria.com", first_name="James Bond")
        self.create_user(24136, agent=agent)
        self.booking = Booking(
            status=Booking.SCHEDULED, user=self.user,
            first_session=datetime.datetime(2017, 3, 10, 12, 30),
            last_session=datetime.datetime(2017, 4, 9, 12, 30,),
            total_price=Decimal(10000), tutor=self.user
        )

    def get_response_for_pipeline(self):
        return self.mock_response(
            [{
                "pipelineId": "f9c18514-7681-4130-afc2-4f487ee9f1de",
                "stages": [
                    {
                        "stageId": "e17d64d3-9e80-4dc4-8d85-20cc1f1eba79",
                        "label": "Booking Created",
                    },
                    {
                        "stageId": "5b39702f-e4eb-4736-a1e0-d416adbf4e8b",
                        "label": "Booking Delivered",
                    },
                    {
                        "stageId": "6549f8f8-3ecf-45dd-a88f-eafb1eddaa2d",
                        "label": "Booking Closed",
                    },
                    {
                        "stageId": "5c6fd2af-6be0-4f66-b10a-b28e6a188eda",
                        "label": "Booking Cancelled",
                    }
                ],
                "label": "Booking Pipeline",
            }, ], status_code=200, overwrite=True
        )

    def assert_booking_creation(self, status, extras=None, deal_type=1):
        self.assertTrue(self.booking.status == status)
        self.assertIsNone(self.booking.dealId)
        self.mock_get.side_effect = [
            self.get_response_for_pipeline()]
        side_effects = []
        if extras:
            side_effects.extend([extras])
        side_effects.append(
            self.mock_response(
                {
                    "portalId": 12232,
                    "dealId": 151088,
                    "isDeleted": False,
                    "associations": {
                        "associatedVids": [
                            24136
                        ],

                    }, }, statuss_code=200, overwrite=True
            ),
        )
        self.mock_post.side_effect = side_effects
        self.booking.create_new_deal(
            self.instance, self.agent, deal_type=deal_type)
        calls = [
        ]
        if extras:
            calls.append(
                mock.call('https://api.hubapi.com/contacts/v1/contact/',
                          headers={
                              'content-type': 'application/json'},
                          json={'properties': [
                              {'property': 'email',
                                           'value': self.booking.user.email},
                              {'property': 'firstname', 'value': 'James'},
                              {'property': 'lastname', 'value': 'Novac'},
                              {'property': 'mobilephone',
                                           'value': '+2347035209976'},
                              {'property': 'lifecyclestage', 'value': 'customer'}]},
                          params={'hapikey': '5d47d98b-6f47-459a-a7fe-7980de321a2e'}))
        calls.extend([
            self.deal_creation_response(deal_type=deal_type)])
        self.mock_post.assert_has_calls(calls)
        self.assert_pipeline_called()
        self.assertEqual(self.booking.dealId, 151088)

    def test_creating_a_booking_with_a_scheduled_status_populate_the_fields(self):
        # Check the created date
        # check the status of pipeline
        self.assert_booking_creation(Booking.SCHEDULED)

    @property
    def get_status(self):
        return {
            Booking.SCHEDULED: "e17d64d3-9e80-4dc4-8d85-20cc1f1eba79",
            Booking.COMPLETED: "6549f8f8-3ecf-45dd-a88f-eafb1eddaa2d",
            Booking.DELIVERED: "5b39702f-e4eb-4736-a1e0-d416adbf4e8b",
            Booking.CANCELLED: "5c6fd2af-6be0-4f66-b10a-b28e6a188eda"
        }

    def deal_creation_response(self, deal_type=1):
        status = self.get_status
        return mock.call(
            "{}/deals/v1/deal/".format(self.instance.call_url),
            json={
                'associations': {
                    "associatedVids": [
                        24136
                    ]
                },
                'properties': [
                    get_value('amount', "10000"),
                    get_value('closedate', as_timestamp(
                        self.booking.last_session)),
                    get_value('createdate', as_timestamp(
                        self.booking.first_session)),
                    get_value('hubspot_owner_id', "222"),
                    get_value("dealtype", "newbusiness" if deal_type ==
                              1 else "existingbusiness"),
                    get_value('description',
                              self.booking.tutor.email),
                    get_value("days_per_week", "4 weeks"),
                    get_value("dealname",
                              "Booking With James Novac", ),
                    get_value(
                        "dealstage", status[self.booking.status]),
                    get_value(
                        'pipeline', "f9c18514-7681-4130-afc2-4f487ee9f1de"),
                ]
            },
            params={
                'hapikey': self.instance.api_key
            },
            headers={
                'content-type': 'application/json'
            }
        )

    def assert_pipeline_called(self):
        self.assertTrue(HubspotStorage.objects.first(
        ).booking_pipeline_id, "f9c18514-7681-4130-afc2-4f487ee9f1de")
        self.mock_get.assert_has_calls(
            [
                mock.call(
                    "{}/deals/v1/pipelines".format(
                        self.instance.call_url),
                    params={
                        'hapikey': self.instance.api_key
                    },
                    headers={
                        'content-type': 'application/json'
                    })])

    def test_creating_a_booking_with_a_completed_status(self):
        # Check the created date
        self.booking.status = Booking.COMPLETED
        self.assert_booking_creation(Booking.COMPLETED)

    def test_creating_a_booking_with_a_delivered_status(self):
        # Check the created date
        # check the status of pipeline
        self.booking.status = Booking.DELIVERED
        self.assert_booking_creation(Booking.DELIVERED)

    def test_updating_a_booking_with_a_cancelled_status(self):
        self.booking.status = Booking.CANCELLED
        self.assert_booking_creation(Booking.CANCELLED)

    def assertion_on_update(self, text, paid=True, owing=False):
        self.mock_post.return_value = self.mock_response(
            {}, status_code=200, overwrite=True)

        self.mock_put.return_value = self.mock_response(
            {
                "portalId": 62515,
                "dealId": 15582,
            }, status_code=200, overwrite=True
        )
        self.mock_get.side_effect = [
            self.get_response_for_pipeline()]

        self.instance.update_booking_deal(
            self.booking, field='dealId', paid=paid, owing=owing
        )
        properties = [
            get_value('amount', "10000"),
            get_value(
                "dealstage", self.get_status[self.booking.status]),
            get_value(
                'pipeline', "f9c18514-7681-4130-afc2-4f487ee9f1de"),
        ]
        if paid:
            properties.append(
                get_value('payment_schedule', text),
            )
        self.mock_put.assert_called_once_with(
            "{}/deals/v1/deal/15582".format(self.instance.call_url),
            json={'properties': properties,
                  }, params={
                'hapikey': self.instance.api_key
            },
            headers={
                'content-type': 'application/json'
            }
        )
        self.mock_post.assert_called_once_with(
            "{}/contacts/v1/contact/vid/{}/profile".format(
                self.instance.call_url, self.booking.user.vid),
            json={
                'properties': [
                    {'property': 'hs_lead_status',
                        'value': 'OWING' if owing else 'CUSTOMER'}
                ]
            },
            params={
                'hapikey': self.instance.api_key
            },
            headers={
                'content-type': 'application/json'
            }
        )

    @mock.patch('hubspot.timezone.now')
    def test_when_booking_is_paid_for_before_start_of_class(self, mock_now):
        # check the payment schedule
        # check the lead status of customer
        mock_now.return_value = datetime.datetime(
            2017, 3, 9, 10, 11, tzinfo=pytz.UTC)
        self.booking.dealId = 15582
        self.assertion_on_update("Instant")

    @mock.patch('hubspot.timezone.now')
    def test_when_booking_is_paid_for_before_end_of_class(self, mock_now):
        # check the payment schedule
        # check the lead status of customer
        mock_now.return_value = datetime.datetime(
            2017, 3, 15, 10, 11, tzinfo=pytz.UTC)
        self.booking.dealId = 15582
        self.assertion_on_update("Flexible")

    @mock.patch('hubspot.timezone.now')
    def test_when_booking_is_paid_for_after_class_end(self, mock_now):
        # check the payment schedule
        # check the lead status of customer
        mock_now.return_value = datetime.datetime(
            2017, 4, 10, 10, 11, tzinfo=pytz.UTC)
        self.booking.dealId = 15582
        self.assertion_on_update("End of Lesson")

    def test_when_contact_doesnt_exist_for_client(self):
        # create contact hubspot id
        # create booking and save deal_id
        self.booking.user.vid = None
        self.assertIsNone(self.booking.user.vid)
        HubspotContact.objects.all().delete()
        self.assert_booking_creation(Booking.SCHEDULED, extras=self.mock_response({
            'vid': 24136
        }, status_code=200, overwrite=True))
        self.assertTrue(HubspotContact.objects.count(), 1)

    def test_status_of_customer_on_husbpot_changes_on_payment_made(self):
        # update the payment schedule
        # update the lead status of customer
        self.booking.dealId = 15582
        self.assertion_on_update("End of Lesson", owing=True, paid=False)

    def test_deal_type_is_set_based_on_single_or_multiple_booking_occurrence(self):
        # check the deal type and ensure it is set to repeat business

        self.assert_booking_creation(Booking.SCHEDULED, deal_type=2)

    def create_booking(self):
        user1 = BaseRequestTutor()
        self.booking = Booking(
            user=NewUser(email="j@example.com", vid=32342),
            total_price=7000,
            status=1,
            created=datetime.datetime(2017, 8, 4, 11, 12)
        )

    def create_user(self, vid=None, agent=None):
        self.agent = agent
        self.instance.initialize_hubspot()
        self.user = NewUser(
            first_name="James", last_name="Novac",
            phone_number_details="+2347035209976", home_address="20 irabor", state="Lagos",
            email="oo@example.com", status=1,
            vicinity="ibadan",
            where_you_heard="from google",
            region='',
            created=datetime.datetime(2017, 10, 3, 12, 11, 2, tzinfo=pytz.utc),
            modified=datetime.datetime(
                2017, 10, 5, 12, 11, 2, tzinfo=pytz.utc),
            budget=Decimal(50000),
            request_subjects=["Mathematics", "English"],
            vid=vid,
            expectation="I want my child to succed.",
            time_of_lesson="5:30pm",
            days_per_week=4,
            pk=33,
            class_of_child="Adult",
            curriculum="",
            gender="M",
            hours_per_day=7,
            available_days=["Monday", "Tuesday"],
            request_type="parent",
            agent=self.agent)
        HubspotContact.objects.create(email=self.user.email, vid=vid)


def get_value(key, value):
    return {
        'name': key,
        'value': value}


class ActualUsageTestCase(TestCase):
    def setUp(self):
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        agent = Agent(email="biola@tuteria.com", first_name="Abiola Oyeniyi")
        self.create_user(agent=agent)
        self.instance.get_all_ids_and_delete(
            [self.user.email])
        self.create_user(agent=agent)
        self.booking = Booking(
            status=Booking.SCHEDULED, user=self.user,
            first_session=datetime.datetime(2017, 3, 10, 12, 30),
            last_session=datetime.datetime(2017, 4, 9, 12, 30,),
            total_price=Decimal(10000), tutor=self.user
        )

    def test_create_deal(self):
        self.assertTrue(HubspotOwner.objects.count() > 0)
        self.assertTrue(HubspotContact.objects.count() == 0)
        self.assertIsNotNone(self.user.agent.hubspot_id)
        self.booking.create_new_deal(self.instance, self.agent)
        self.assertIsNotNone(self.booking.dealId)
        self.assertTrue(HubspotContact.objects.count() > 0)
        response = self.instance.update_booking_deal(
            self.booking, paid=True, owing=True
        )
        self.assertIsNotNone(response)

    def create_user(self, vid=None, agent=None):
        self.agent = agent
        self.instance.initialize_hubspot()
        self.user = BaseRequestTutor(
            first_name="James", last_name="Novac",
            phone_number_details="+2347035209976", home_address="20 irabor", state="Lagos",
            email="oo@example.com", status=1,
            vicinity="ibadan",
            where_you_heard="from google",
            region='',
            created=datetime.datetime(2017, 10, 3, 12, 11, 2, tzinfo=pytz.utc),
            modified=datetime.datetime(
                2017, 10, 5, 12, 11, 2, tzinfo=pytz.utc),
            budget=Decimal(50000),
            request_subjects=["Mathematics", "English"],
            vid=vid,
            expectation="I want my child to succed.",
            time_of_lesson="5:30pm",
            days_per_week=4,
            pk=33,
            class_of_child="Adult",
            curriculum="",
            gender="M",
            hours_per_day=7,
            available_days=["Monday", "Tuesday"],
            request_type="parent",
            agent=self.agent)
