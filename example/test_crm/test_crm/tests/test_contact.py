from __future__ import unicode_literals
from builtins import super, str

from django.shortcuts import reverse
from hubspot import HubspotAPI, HubspotAPIException, get_property_array
from hubspot.models import HubspotStorage, HubspotOwner
import datetime
from . import BaseTestCase, mock, TestCase, BaseRequestTutor,Agent
from collections import OrderedDict


class SharedMixin(object):
    def create_user(self, email="j@example.com", agent=None):
        params = dict(first_name="James", last_name="Novac",
                      number="+2347035209976", home_address="20 irabor", state="Lagos",
                      email=email, status=1, vicinity=None, region=None,
                      where_you_heard="google",agent=agent)
        return BaseRequestTutor(**params)

    def create_users(self, agent=None):
        user1 = self.create_user(agent=agent)
        user2 = self.create_user(email="shola@example.com",agent=agent)
        return user1, user2

    def get_response(self, agent=None):
        user1, user2 = self.create_users(agent=agent)
        self.mock_get.return_value = self.mock_response(
            OrderedDict({
                '101': {
                    "identity-profiles": [
                        {
                            "vid": 101,
                            "identities": [
                                {
                                    "type": "LEAD_GUID",
                                    "value": "90511268-9778-4563-96c3-36beb2319755",

                                },
                                {
                                    "type": "EMAIL",
                                    "value": user1.email
                                }
                            ]
                        }
                    ],
                },
                '102': {
                    "identity-profiles": [
                        {
                            "vid": 102,
                            "identities": [
                                {
                                    "type": "LEAD_GUID",
                                    "value": "90511268-9778-4563-96c3-36beb2319755",

                                },
                                {
                                    "type": "EMAIL",
                                    "value": user2.email
                                }
                            ]
                        }
                    ],
                }
            }), status_code=200, overwrite=True
        )
        return user1, user2


class ContactCreationTestCase(SharedMixin, BaseTestCase):
    def setUp(self):
        super().setUp()
        self.agent = Agent(email="h@example.com")
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        self.inst = HubspotStorage.objects.create(
            contact_properties=True, fetched_agents=True,
            deal_properties=True
        )
        self.instance.initialize_hubspot()
        self.path = "{}/contacts/v1/contact/".format(
            self.instance.call_url
        )

    def get_properties(self, user):
        properties = get_property_array(
            sorted(self.instance._contact_fields.items()), user, field='property')
        properties.append({
            'property': "lifecyclestage",
            "value": "lead"
        })
        properties.append({
            'property': "hs_lead_status",
            "value": "ISSUED"
        })
        properties.append({
            'property':'hubspot_owner_id',
            "value": str(self.agent.hubspot_id)
        })
        return properties

    def init_user(self, additional_calls=None):
        user = self.create_user(agent=self.agent)
        user.save_vid(self.instance)
        calls = [mock.call(self.path, json={
            'properties': self.get_properties(user)},
            headers={
                'content-type': 'application/json'
        },
            params={
                'hapikey': self.instance.api_key
        })]
        if additional_calls:
            calls.append(additional_calls)
        self.mock_post.assert_has_calls(calls)
        return user

    def test_create_new_contact_new_request(self):
        self.mock_post.return_value = self.mock_response(
            {
                "vid": 61574},
            status_code=200, overwrite=True
        )
        user = self.init_user()
        self.assertEqual(user.vid, 61574)

    def test_create_existing_contact_new_request(self):
        self.mock_post.return_value = self.mock_response(
            {"identityProfile": {
                "vid": 200
            }}, status_code=409, overwrite=True
        )
        user = self.init_user(mock.call(
            "{}vid/200/profile".format(self.path), json={
                'properties': [
                    {
                        'property': "lifecyclestage",
                        'value': "opportunity"
                    },
                    {
                        'property': "hs_lead_status",
                        'value': "PENDING"
                    }
                ]
            }, headers={'content-type': 'application/json'},
            params={
                'hapikey': self.instance.api_key
            }
        ))

    def test_create_new_contact_from_existing_request(self):
        self.mock_get.return_value = self.mock_response(
            {
                'vid': 200
            }, status_code=200, overwrite=True
        )
        user = self.create_user()
        id_ = self.instance.get_contact(user)
        self.mock_get.assert_called_once_with(
            "{}email/{}/profile".format(self.path, user.email),
            headers={
                'content-type': "application/json"
            },
            params={
                'hapikey': self.instance.api_key
            }
        )
        self.mock_post.assert_not_called()

    def test_bulk_creation_of_contacts_with_bulk_update(self):
        user1, user2 = self.get_response(agent=self.agent)

        self.assertIsNone(user1.vid)
        self.assertIsNone(user2.vid)
        self.mock_get.return_value = self.mock_response(
            {
                "contacts": [
                    {
                        "identity-profiles": [
                            {
                                "vid": 101,
                                "identities": [
                                    {
                                        "type": "LEAD_GUID",
                                        "value": "90511268-9778-4563-96c3-36beb2319755",

                                    },
                                    {
                                        "type": "EMAIL",
                                        "value": user1.email
                                    }
                                ]
                            }
                        ],

                    },
                    {
                        "identity-profiles": [
                            {
                                "vid": 102,
                                "identities": [
                                    {
                                        "type": "LEAD_GUID",
                                        "value": "90511268-9778-4563-96c3-36beb2319755",

                                    },
                                    {
                                        "type": "EMAIL",
                                        "value": user2.email
                                    }
                                ]
                            }
                        ],

                    }
                ], }, status_code=200, overwrite=True
        )
        self.mock_post.return_value = self.mock_response(
            {}, status_code=202, overwrite=True
        )

        def callback_func(result):
            for aa in [user1, user2]:
                for oo in result:
                    if aa.email == oo['email']:
                        for key, value in oo.items():
                            setattr(aa, key, value)
        self.instance.create_bulk_contacts(
            [user1, user2], bulk_callback=callback_func)
        self.assertTrue(self.mock_post.called)
        self.mock_get.assert_called_once_with(
            "{}/contacts/v1/lists/all/contacts/recent".format(
                self.instance.call_url),
            params={
                'count': 100,
                'hapikey': self.instance.api_key
            },
            headers={
                'content-type': 'application/json'
            }
        )
        self.assertEqual(user1.vid, 101)
        self.assertEqual(user2.vid, 102)

    def test_bulk_creation_of_contacts(self):
        user1, user2 = self.get_response(agent=self.agent)

        self.assertIsNone(user1.vid)
        self.assertIsNone(user2.vid)
        self.mock_post.return_value = self.mock_response(
            {}, status_code=202, overwrite=True
        )

        def callback_func(xx):
            instance = [u for u in [user1, user2] if u.email == xx['email']][0]
            instance.vid = xx['vid']
        self.instance.create_bulk_contacts(
            [user1, user2], callback=callback_func)
        self.mock_post.assert_called_once_with(
            "{}batch/".format(self.path),
            json=[{
                'email': user1.email,
                'properties': self.get_properties(user1)
            },
                {'email': user2.email,
                 'properties': self.get_properties(user2)}],
            headers={
                'content-type': 'application/json'
            },
            params={
                'hapikey': self.instance.api_key
            }
        )
        self.mock_get.assert_called_once_with(
            "{}emails/batch/".format(self.path),
            params={
                'email': [user1.email, user2.email],
                'hapikey': self.instance.api_key
            },
            headers={
                'content-type': 'application/json'
            }
        )
        self.assertEqual(user1.vid, 101)
        self.assertEqual(user2.vid, 102)

    def test_delete_contact(self):
        self.mock_delete.return_value = self.mock_response(
            {
                "vid": 61571,
                "deleted": True,
                "reason": "OK"
            }, status_code=200, overwrite=True
        )
        self.instance.delete_contact(61571)
        self.mock_delete.assert_called_once_with(
            "{}vid/61571".format(self.path),
            params={
                'hapikey': self.instance.api_key
            },
            headers={
                'content-type': 'application/json'
            }
        )

    def test_get_all_ids_and_delete(self):
        user1, user2 = self.get_response()
        self.mock_delete.return_value = self.mock_response(
            {}
        )
        self.instance.get_all_ids_and_delete([user1.email, user2.email])
        self.assertTrue(self.mock_get.called)
        self.mock_delete.assert_has_calls([
            mock.call('{}vid/{}'.format(self.path, vid), params={
                'hapikey': self.instance.api_key
            },
                headers={
                'content-type': 'application/json'
            }) for vid in [101, 102]
        ])


class ActualTestCase(SharedMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))
        self.agent = Agent(email="biola@tuteria.com", first_name="Abiola Oyeniyi")
        self.user1, self.user2 = self.create_users(agent=self.agent)
        self.instance.get_all_ids_and_delete(
            [self.user1.email, self.user2.email])
        # self.instance._delete_deal_group('lesson_information_details')

    def test_bulk_create(self):
        values = []

        self.assertIsNone(self.user1.vid)
        self.assertIsNone(self.user2.vid)

        def callback(result):
            values.extend(result)

        self.instance.create_bulk_contacts(
            [self.user1, self.user2], bulk_callback=callback)
        self.user1.vid = [
            x['vid'] for x in values if x['email'] == self.user1.email]
        self.user2.vid = [
            x['vid'] for x in values if x['email'] == self.user2.email]
        self.assertIsNotNone(self.user1.vid)
        self.assertIsNotNone(self.user2.vid)

    def test_create_contact(self):
        self.assertIsNone(self.user1.vid)
        self.instance.initialize_hubspot()
        self.assertIsNotNone(self.agent.hubspot_id)
        result = self.instance.create_contact(self.user1)
        self.user1.vid = result
        self.assertIsNotNone(self.user1.vid)

        response = self.instance.update_contact(request_object=self.user1)
        self.assertTrue(response)

    def test_update_contact(self):
        pass

