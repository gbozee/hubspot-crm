from builtins import super
try:
    from unittest import mock
except ImportError:
    import mock
from django.shortcuts import reverse
from hubspot import HubspotAPI, HubspotAPIException
from hubspot.models import HubspotStorage
import datetime
from . import BaseTestCase


class HubspotAPITestCase(BaseTestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        self.instance = HubspotAPI(reverse('hubspot:hubspot_code'))

    def test_authorization_url_returns_valid_value(self):
        self.assertEqual(self.instance.get_authorization_url(),
                         "https://app.hubspot.com/oauth/authorize?redirect_uri={}&client_id={}&scope={}".format(
            self.instance.redirect_url,
            self.instance.client_id,
            "contacts%20timeline%20files"
        ))

    def test_get_token_on_success(self):
        solution = self.initialize_mock()
        result, status = self.instance.get_token("1234")
        path = "{}/oauth/v1/token".format(self.instance.call_url)
        self.mock_post.assert_called_once_with(
            path, data={
                'grant_type': "authorization_code",
                "client_id": self.instance.client_id,
                "client_secret": self.instance.client_secret,
                "redirect_uri": self.instance.redirect_url,
                "code": "1234"
            },
            headers={
                'content-type': 'application/x-www-form-urlencoded'
            }
        )
        self.assertEqual(result, solution)
        self.assertEqual(status, 200)

    def test_get_token_on_failure(self):
        solution = self.initialize_mock(status=400)
        result, status = self.instance.get_token("1234")
        path = "{}/oauth/v1/token".format(self.instance.call_url)
        self.mock_post.assert_called_once_with(
            path, data={
                'grant_type': "authorization_code",
                "client_id": self.instance.client_id,
                "client_secret": self.instance.client_secret,
                "redirect_uri": self.instance.redirect_url,
                "code": "1234"
            },
            headers={
                'content-type': 'application/x-www-form-urlencoded'
            }
        )
        self.assertEqual(result, solution)
        self.assertEqual(status, 400)

    def test_fetch_token_from_db_when_it_exists_and_not_expired(self):
        result = HubspotStorage.save_token(
            access_token="1232322", refresh_token="ewojweoiwe")
        self.assertFalse(result.has_expired())
        self.assertEqual(self.instance.fetch_token_from_db(), "1232322")

    def expired_token(self):
        result = HubspotStorage.save_token(
            access_token="1232322", refresh_token="ewojweoiwe")
        result.date_added = result.date_added - \
            datetime.timedelta(minutes=60 * 7)
        result.save()
        self.assertTrue(result.has_expired())

    def test_fetch_token_from_db_when_it_exists_and_it_is_expired_success(self):
        self.expired_token()
        self.initialize_mock(
            status=200, access_token="new_token", refresh_token="new_refresh_token")
        self.assertEqual(self.instance.fetch_token_from_db(), "new_token")
        self.assertTrue(self.mock_post.called)
        old_token = HubspotStorage.get_token()
        self.assertEqual(old_token.access_token, "new_token")

    def test_fetch_token_from_db_when_it_exists_and_it_is_expired_failed(self):
        self.expired_token()
        solution = self.initialize_mock(status=400)
        with self.assertRaises(HubspotAPIException) as context:
            self.instance.fetch_token_from_db()

        self.assertEqual('code: {}, description: {}'.format(
            solution['error'], solution['error_description']), str(context.exception))

    def test_fetch_token_from_db_when_it_doesnt_exist(self):
        self.assertEqual(HubspotStorage.objects.count(), 0)
        with self.assertRaises(HubspotAPIException) as context:
            self.instance.fetch_token_from_db()
        self.assertTrue("Token not available, please visit the {} url.".format(
            reverse("hubspot:hubspot_code")))


class HubspotViewTestCase(BaseTestCase):

    def test_auth_response_saves_to_db_when_successful(self):
        self.assertEqual(HubspotStorage.objects.count(), 0)
        solution = self.initialize_mock()
        url = reverse('hubspot:hubspot_code')
        response = self.client.get(url, {
            'code': "abcde"
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(HubspotStorage.objects.count(), 1)
        token = HubspotStorage.get_token()
        self.assertEqual(token.access_token, solution['access_token'])
        self.assertEqual(token.refresh_token, solution['refresh_token'])

    def test_auth_response_when_error(self):
        self.assertEqual(HubspotStorage.objects.count(), 0)
        self.initialize_mock(status=400)
        url = reverse('hubspot:hubspot_code')
        response = self.client.get(url, {
            'code': "abcde"
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(HubspotStorage.objects.count(), 0)
