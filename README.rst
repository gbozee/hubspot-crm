===============
Tuteria Hubspot
================

Hubspot is a simple Django app to create contacts and deals from django to hubspot as well
as create notes and notify hubspot owners when actions happen after a 
webhook is called.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "polls" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'hubspot',
    ]

2. Include the polls URLconf in your project urls.py like this::

    url(r'^hubspot/', include('hubspot.urls',namespace='hubspot')),

3. Run `python manage.py migrate` to create the polls models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a poll (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/polls/ to participate in the poll.

6. Implement the callback for the 
`hubspot.signals.hubspot_action_after_message_webhook` receiver for actions that should happen when webhook is received.
